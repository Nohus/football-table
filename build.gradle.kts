plugins {
    kotlin("multiplatform") version "1.6.0"
}

repositories {
    mavenCentral()
}

kotlin {
    linuxArm32Hfp("native") { // Raspberry Pi architecture
        compilations.getByName("main") {
            cinterops {
                register("pigpio")
            }
            dependencies {

            }
        }
        binaries {
            executable {
               entryPoint = "table.main"
            }
        }
    }
    sourceSets {
        all {
            languageSettings.optIn("kotlin.ExperimentalUnsignedTypes")
        }
        getByName("nativeMain") {

        }
    }
}
