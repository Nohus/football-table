#!/bin/bash
rsync --recursive --delete\
  sound\
  table:/home/pi/table
rsync --recursive --delete\
  sound.sh\
  table:/home/pi/table
rsync --recursive --delete\
  soundloop.sh\
  table:/home/pi/table
