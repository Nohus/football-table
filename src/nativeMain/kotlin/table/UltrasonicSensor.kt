package table

import platform.posix.usleep
import table.Wire.Mode.INPUT
import table.Wire.Mode.OUTPUT
import table.Wire.State.HIGH
import table.Wire.State.LOW
import kotlin.system.measureTimeMicros

class UltrasonicSensor(
    private val triggerPin: Int,
    private val echoPin: Int
) {

    companion object {
        private const val BUFFER_SIZE = 500
        private const val MIN_MEASUREMENT_INTERVAL = 500UL // In microseconds
        private const val MAX_DISTANCE = 500L // In millimeters
        private val MAX_DURATION = distanceToDuration(MAX_DISTANCE).toULong()

        private fun durationToDistance(duration: Long): Long {
            return duration * 17_150 / 100_000 // Sound time of flight to distance calculation
        }

        private fun distanceToDuration(distance: Long): Long {
            return distance * 100_000 / 17_150
        }
    }

    private val measurements = mutableListOf<Long>()
    private var cachedBaseline: Long? = null

    init {
        Wire.setMode(triggerPin, OUTPUT)
        Wire.setMode(echoPin, INPUT)
    }

    fun getNewDistance(measurementsAmount: Int): Long {
        val measurements = mutableListOf<Long>()
        while (measurements.size < measurementsAmount) {
            getDistanceMillimeters()?.let { measurements += it }
            usleep(MIN_MEASUREMENT_INTERVAL.toUInt())
        }
        return measurements.takeLast(measurementsAmount).sorted()[measurementsAmount / 2]
    }

    fun getBaseLine(): Long {
        cachedBaseline?.let { return it }
        fillBuffer()
        return measurements.sorted()[measurements.size / 2].also { cachedBaseline = it }
    }

    fun getDistance(measurementsAmount: Int): Long {
        updateMeasurements()
        return measurements.takeLast(measurementsAmount).sorted()[measurementsAmount / 2]
    }

    fun getDistanceDebug(measurementsAmount: Int): Pair<Long, List<Long>> {
        updateMeasurements()
        val taken = measurements.takeLast(measurementsAmount)
        val median = taken.sorted()[measurementsAmount / 2]
        return median to taken
    }

    private fun updateMeasurements() {
        if (measurements.isNotEmpty()) {
            measurements.removeAt(0)
        }
        fillBuffer()
    }

    private fun fillBuffer() {
        while (measurements.size < BUFFER_SIZE) {
            getDistanceMillimeters()?.let {
                measurements += it
                cachedBaseline = null
            }
            usleep(MIN_MEASUREMENT_INTERVAL.toUInt())
        }
    }

    private fun getDistanceMillimeters(): Long? {
        val measurementTimeout = Timer.ofMillis(1)
        val durationTimeout = Timer.ofMicros(MAX_DURATION, false)
        pulseTrigger()
        while (Wire.read(echoPin) == LOW) {
            if (measurementTimeout.isReached()) return null // Did not get a measurement
        }
        durationTimeout.start()
        val duration = measureTimeMicros {
            while (Wire.read(echoPin) == HIGH) {
                if (durationTimeout.isReached()) return MAX_DISTANCE
            }
        }
        val distance = durationToDistance(duration)
        if (distance <= 10) return null // Did not get a valid measurement
        return distance
    }

    private fun pulseTrigger() {
        // First ensure the pin is low for a moment so the high signal is clean
        Wire.write(triggerPin, LOW)
        usleep(5)
        // Make the signal high for 10 microseconds, which triggers the sensor
        Wire.write(triggerPin, HIGH)
        usleep(10)
        Wire.write(triggerPin, LOW)
    }

}
