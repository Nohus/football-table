package table

import table.Wire.Mode.INPUT
import table.Wire.Mode.OUTPUT
import table.Wire.PullResistor.*
import table.Wire.State.HIGH
import table.Wire.State.LOW
import pigpio.*

/**
 * Thread-safe wrapper over the wiringPi library
 */
object Wire {

    enum class State {
        HIGH, LOW
    }

    enum class Mode {
        INPUT, OUTPUT
    }

    enum class PullResistor {
        PULL_UP, PULL_DOWN, OFF
    }

    init {
        gpioInitialise()
    }

    fun setMode(pin: Int, mode: Mode) {
        val pigpioMode = when (mode) {
            INPUT -> PI_INPUT
            OUTPUT -> PI_OUTPUT
        }
        gpioSetMode(pin.toUInt(), pigpioMode.toUInt())
    }

    fun setPullResistor(pin: Int, mode: PullResistor) {
        val pigpioMode = when (mode) {
            PULL_UP -> PI_PUD_UP
            PULL_DOWN -> PI_PUD_DOWN
            OFF -> PI_PUD_OFF
        }
        gpioSetPullUpDown(pin.toUInt(), pigpioMode.toUInt())
    }

    fun write(pin: Int, state: State) {
        gpioWrite(pin.toUInt(), if (state == HIGH) 1u else 0u)
    }

    fun read(pin: Int): State {
        return when (gpioRead(pin.toUInt())) {
            0 -> LOW
            1 -> HIGH
            else -> throw AssertionError("Illegal GPIO read state")
        }
    }

}
