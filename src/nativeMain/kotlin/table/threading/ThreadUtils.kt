package table.threading

import kotlinx.cinterop.COpaquePointer
import kotlinx.cinterop.asStableRef

fun threadMain(input: COpaquePointer?): COpaquePointer? {
    initRuntimeIfNeeded()
    input!!.asStableRef<() -> Unit>().apply {
        val block = get()
        block()
        dispose()
    }
    return null
}

fun Collection<Thread>.joinAll() {
    forEach { it.join() }
}
