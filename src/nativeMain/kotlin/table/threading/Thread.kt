package table.threading

import kotlinx.cinterop.*
import platform.posix.*

class Thread(block: () -> Unit) {

    private var thread: pthread_tVar = nativeHeap.alloc()
    private val blockReference = StableRef.create(block)

    fun start() = apply {
        pthread_create(thread.ptr, null, staticCFunction(::threadMain), blockReference.asCPointer())
    }

    fun cancel() {
        pthread_cancel(thread.value)
        destroy()
    }

    fun join() {
        pthread_join(thread.value, null)
        destroy()
    }

    private fun destroy() {
        nativeHeap.free(thread)
    }

}
