package table.threading

import kotlinx.cinterop.cValue
import platform.posix.*

class Lock {

    private val mutex = cValue<pthread_mutex_t>()

    init {
        pthread_mutex_init(mutex, null)
    }

    fun lock() {
        pthread_mutex_lock(mutex)
    }

    fun unlock() {
        pthread_mutex_unlock(mutex)
    }

    fun destroy() {
        pthread_mutex_destroy(mutex)
    }

    fun <T> use(block: () -> T): T {
        try {
            lock()
            return block()
        } finally {
            unlock()
        }
    }

}
