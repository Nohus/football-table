package table

import kotlinx.cinterop.*
import platform.posix.fscanf
import platform.posix.pclose
import platform.posix.popen
import platform.posix.system

class SoundPlayer {

    /**
     * Needs "sox", and "libsox-fmt-all" installed on the system
     */
    fun play(vararg names: String, loop: Boolean = false): Int {
        val filenames = names.joinToString(" ") { "./sound/$it.mp3" }
        val command = "./sound.sh \"$filenames\" ${if (loop) "loop" else ""}"
        val file = popen(command, "r")
        memScoped {
            val pid = alloc<IntVar>()
            fscanf(file, "%d", pid.ptr)
            pclose(file)
            return pid.value
        }
    }

    fun stop(id: Int) {
        system("pkill -P $id; kill $id")
    }

}
