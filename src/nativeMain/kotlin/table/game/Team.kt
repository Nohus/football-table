package table.game

enum class Team {

    Red, Blue;

    fun other() = when (this) {
        Red -> Blue
        Blue -> Red
    }

}
