package table.game

import table.SoundPlayer
import table.display.Display
import table.display.animations.AnimatedObjects
import kotlin.math.max
import kotlin.system.getTimeMillis

/**
 * Created by Marcin Wisniowski (Nohus) on 01/03/2020.
 */

class SpecialEffects(
    private val scores: Map<Team, Int>,
    private val goalHistory: List<Team>,
    private val soundPlayer: SoundPlayer,
    private val display: Display
) {

    var specialEffectEndTimestamp: Long = 0
        private set
    private var loopSound: Int? = null

    data class Message(
        val scorerText: String?,
        val notScorerText: String?
    )

    data class Effect(
        val message: Message,
        val sound: String
    )

    fun handleSpecialEffects() {
        val scoreEffect = getScoreSpecialEffect()
        val situationEffect = getSituationSpecialEffect()

        if (getTimeToSpecialEffectEnd() == 0L) {
            // Only clear the display if there are no other effects running
            display.canvas.clear()
        }
        displaySpecialEffectMessage(scoreEffect.message)
        situationEffect?.message?.let {
            displaySpecialEffectMessage(situationEffect.message)
        }

        soundPlayer.play("unreal/${scoreEffect.sound}")
        situationEffect?.sound?.let { backgroundMusic ->
            loopSound = soundPlayer.play("music/$backgroundMusic", loop = true)
        }
    }

    fun stopBackgroundMusic() {
        loopSound?.let {
            soundPlayer.stop(it)
            loopSound = null
        }
    }

    private fun getSituationSpecialEffect(): Effect? {
        return when {
            scores.getValue(Team.Red) >= 10 || scores.getValue(Team.Blue) >= 10 -> {
                null // A team has won, effects off
            }
            scores[Team.Red] == 9 && scores[Team.Blue] == 9 -> { // 9:9 points
                Effect(Message(null, null), "9-9")
            }
            scores[Team.Red] == 9 && scores[Team.Blue] == 0 -> { // 9:0 points for red
                Effect(Message("ONE MORE", "GOOD LUCK"), "9-0")
            }
            scores[Team.Blue] == 9 && scores[Team.Red] == 0 -> { // 9:0 points for blue
                Effect(Message("ONE MORE", "GOOD LUCK"), "9-0")
            }
            scores[Team.Red] == 9 || scores[Team.Blue] == 9 -> { // 9:X points
                Effect("MATCH BALL".toBoth(), "9-x")
            }
            else -> null
        }
    }

    private fun getScoreSpecialEffect(): Effect {
        return when {
            goalHistory.endsWithRepetitions(10) -> { // 10:0 win
                Effect(Message("FLAW LESS WIN", "DEFEAT"), "flawless_victory")
            }
            scores[Team.Red] == 10 -> { // Red team win
                Effect(Message("YOU WIN", "DEFEAT"), "red_team_is_the_winner")
            }
            scores[Team.Blue] == 10 -> { // Blue team win
                Effect(Message("YOU WIN", "DEFEAT"), "blue_team_is_the_winner")
            }
            scores[Team.Red] == 9 && scores[Team.Blue] == 9 -> { // 9:9 points
                Effect("LAST MAN STAN DING".toBoth(), "last_man_standing")
            }
            (scores[Team.Red] == 9 || scores[Team.Blue] == 9) && isPerfectWinDenied() -> { // Perfect score streak broken at last moment
                Effect(Message("FLAW LESS WIN AVERTED", "FLAW LESS WIN LOST"), "narrowly_averted")
            }
            isPerfectWinDenied() -> { // Perfect score streak broken
                Effect(Message("FLAW LESS WIN AVERTED", "FLAW LESS WIN LOST"), "averted")
            }
            isComeback() -> { // Comeback (equal points after a disadvantage of at least 3 points)
                Effect("COME BACK".toBoth(), "lostlead")
            }
            goalHistory.size == 1 -> { // First goal of the game
                Effect("FIRST BLOOD".toBoth(), "firstblood")
            }
            goalHistory.endsWithRepetitions(9) -> { // Streak of 9
                Effect("GOD LIKE".toBoth(), "godlike")
            }
            goalHistory.endsWithRepetitions(8) -> { // Streak of 8
                Effect("LUDI CROUS KILL".toBoth(), "ludicrouskill")
            }
            goalHistory.endsWithRepetitions(7) -> { // Streak of 7
                Effect("MONS TER KILL".toBoth(), "monsterkill")
            }
            goalHistory.endsWithRepetitions(6) -> { // Streak of 6
                Effect("ULTRA KILL".toBoth(), "ultrakill")
            }
            goalHistory.endsWithRepetitions(5) -> { // Streak of 5
                Effect("MEGA KILL".toBoth(), "megakill")
            }
            goalHistory.endsWithRepetitions(4) -> { // Streak of 4
                Effect("MULTI KILL".toBoth(), "multikill")
            }
            goalHistory.endsWithRepetitions(3) -> { // Streak of 3
                Effect("TRIPLE KILL".toBoth(), "triplekill")
            }
            isTeamDominating(Team.Red) -> {
                Effect(Message("DOMIN ATING", "DOMIN ATED"), "red_team_dominating")
            }
            isTeamDominating(Team.Blue) -> {
                Effect(Message("DOMIN ATING", "DOMIN ATED"), "blue_team_dominating")
            }
            hasTeamIncreasedLead(Team.Red) -> {
                Effect("GOAL!".toBoth(), "red_team_increases_their_lead")
            }
            hasTeamIncreasedLead(Team.Blue) -> {
                Effect("GOAL!".toBoth(), "blue_team_increases_their_lead")
            }
            hasTeamTakenLead(Team.Red) -> {
                Effect(Message("TOOK LEAD", "LOST LEAD"), "red_team_takes_the_lead")
            }
            hasTeamTakenLead(Team.Blue) -> {
                Effect(Message("TOOK LEAD", "LOST LEAD"), "blue_team_takes_the_lead")
            }
            else -> { // Normal goal
                Effect("GOAL!".toBoth(), when (goalHistory.last()) {
                    Team.Red -> "red_team_scores"
                    Team.Blue -> "blue_team_scores"
                })
            }
        }
    }

    private fun String.toBoth() = Message(this, this)

    private fun isComeback(): Boolean {
        if (scores[Team.Red] == scores[Team.Blue]) {
            val team = goalHistory.last()
            val advantageDeltas = goalHistory.map { if (it == team) 1 else -1 }
            val advantageHistory = List(goalHistory.size) { advantageDeltas.take(it + 1).sum() }
            val disadvantageIndex = advantageHistory.indexOfLast { it <= -3 }
            if (disadvantageIndex != -1 && advantageHistory.last() == 0) {
                val comebackPath = advantageHistory.subList(disadvantageIndex + 1, advantageHistory.size - 1)
                if (!comebackPath.contains(0)) return true
            }
        }
        return false
    }

    private fun isPerfectWinDenied(): Boolean {
        if (goalHistory.size <= 5) return false // There is more than 5 goals
        if (goalHistory.first() == goalHistory.last()) return false // And the last goal is different
        return goalHistory.dropLast(1).let { withoutLast -> // While all previous all the same
            withoutLast.all { it == withoutLast.first() }
        }
    }

    private fun isTeamDominating(team: Team): Boolean {
        return hasTeamGainedLeadOf(team, 4)
    }

    private fun hasTeamIncreasedLead(team: Team): Boolean {
        return hasTeamGainedLeadOf(team, 2)
    }

    private fun hasTeamTakenLead(team: Team): Boolean {
        return hasTeamGainedLeadOf(team, 1)
    }

    private fun hasTeamGainedLeadOf(team: Team, minLead: Int): Boolean {
        val lead = scores.getValue(team) - scores.getValue(team.other())
        return goalHistory.last() == team && lead >= minLead
    }

    private fun <T> List<T>.endsWithRepetitions(n: Int): Boolean {
        return size >= n && takeLast(n).all { it == last() }
    }

    private fun displaySpecialEffectMessage(message: Message) {
        var delay = getTimeToSpecialEffectEnd()
        when {
            message.scorerText != null && message.notScorerText != null -> {
                val scorer = goalHistory.last()
                delay = displaySpecialEffectText(message.scorerText, scorer, delay)
                displaySpecialEffectText(message.notScorerText, scorer.other(), delay)
            }
            message.scorerText != null -> displaySpecialEffectText(message.scorerText, goalHistory.last(), delay)
            message.notScorerText != null -> displaySpecialEffectText(message.notScorerText, goalHistory.last().other(), delay)
        }
    }

    private fun displaySpecialEffectText(text: String, team: Team, delay: Long = 0): Long {
        val time = AnimatedObjects.displayTextToTeam(display.canvas, text, team, delay)
        specialEffectEndTimestamp = getTimeMillis() + time
        return time
    }

    private fun getTimeToSpecialEffectEnd() = max(specialEffectEndTimestamp - getTimeMillis(), 0)
}
