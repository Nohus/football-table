package table.game

import kotlinx.cinterop.*
import kotlinx.cinterop.internal.CCall
import platform.posix.*
import table.BUTTON
import table.Button
import table.SoundPlayer
import table.Wire
import table.Wire.State.*
import table.display.Canvas
import table.display.Display
import table.display.DisplayableObject
import table.display.animations.AnimatedObjects
import table.display.bitmaps.Bitmaps
import table.game.Team.Blue
import table.game.Team.Red
import table.game.GameState.*
import kotlin.native.concurrent.AtomicInt
import kotlin.system.getTimeMillis

class Game(
    private val soundPlayer: SoundPlayer,
    private val display: Display
) {

    companion object {
        private const val POINTS_TO_WIN = 10
    }

    private lateinit var state: GameState
    private var winner: Team? = null
    val pendingBlueGoal = AtomicInt(0)
    val pendingRedGoal = AtomicInt(0)
    private val scores = mutableMapOf<Team, Int>()
    private val goalHistory = mutableListOf<Team>()
    private var restartCooldown: Long? = null
    private val button: Button
    private val effects = SpecialEffects(scores, goalHistory, soundPlayer, display)
    private val scoreAnnouncer = ScoreAnnouncer(scores, soundPlayer)

    init {
        button = Button(BUTTON, LOW, {
            println("Button pressed")
            if (state == NotStarted) {
                soundPlayer.play("unreal/prepare")
                changeState(InProgress)
            } else {
                // Pressing the button during the game resets the display
                display.initializeDisplay()
            }
        }) {
            println("Button long pressed")
            if (state == InProgress) reset()
        }
        reset()
    }

    private fun reset() {
        winner = null
        scores[Red] = 0
        scores[Blue] = 0
        goalHistory.clear()
        changeState(NotStarted)
    }

    private fun restart() {
        effects.stopBackgroundMusic()
        restartCooldown = getTimeMillis() + 4_000
        changeState(RestartCooldown)
    }

    fun update() {
        button.update()
        applyPendingGoals()
        applyContinuousStateEffect()
    }

    private fun getScore(team: Team): Int {
        return scores.getValue(team)
    }

    private fun applyPendingGoals() {
        if (pendingBlueGoal.compareAndSet(1, 0)) addGoal(Blue)
        if (pendingRedGoal.compareAndSet(1, 0)) addGoal(Red)
    }

    private fun addGoal(team: Team) {
        if (state !in listOf(NotStarted, InProgress)) return

        println("Goal for $team team")
        if (state == NotStarted) changeState(InProgress)
        scores[team] = getScore(team) + 1
        goalHistory += team

        effects.stopBackgroundMusic()
        if (state == InProgress) {
            effects.handleSpecialEffects()
            checkForWinning()
        }
    }

    private fun checkForWinning() {
        if (getScore(Red) >= POINTS_TO_WIN) setWinner(Red)
        if (getScore(Blue) >= POINTS_TO_WIN) setWinner(Blue)
    }

    private fun setWinner(team: Team) {
        winner = team
        changeState(Finished)
    }

    private fun getScores(perspective: Team): String {
        return when (perspective) {
            Red -> "${getScore(Red)}:${getScore(Blue)}"
            Blue -> "${getScore(Blue)}:${getScore(Red)}"
        }
    }

    private fun changeState(newState: GameState) {
        state = newState
        applyStateTransition()
    }

    private fun applyStateTransition() {
        when (state) {
            NotStarted -> {}
            InProgress -> {}
            Finished -> {
                restart()
            }
            RestartCooldown -> {}
        }
    }

    private fun applyContinuousStateEffect() {
        when (state) {
            NotStarted -> {
                displayClock(display.canvas)
            }
            InProgress -> {
                if (getTimeMillis() > effects.specialEffectEndTimestamp) {
                    //scoreAnnouncer.announceScores()
                    displayScores(display.canvas)
                }
            }
            Finished -> {}
            RestartCooldown -> {
                if (getTimeMillis() >= restartCooldown!!) {
                    restartCooldown = null
                    reset()
                }
            }
        }
    }

    private fun displayScores(canvas: Canvas) {
        display.canvas.clear()
        val scoreNormal = DisplayableObject(Bitmaps.getText(getScores(Blue)))
        val scoreFlipped = DisplayableObject(Bitmaps.getText(getScores(Red)).flipped(), y = 1)
        scoreFlipped.x = 32 - scoreFlipped.width
        canvas.addObject(scoreNormal)
        canvas.addObject(scoreFlipped)
    }

    private fun displayClock(canvas: Canvas) {
        display.canvas.clear()
        val clock = DisplayableObject(Bitmaps.getText(getLocalTime()))
        canvas.addObject(clock)
    }

    private fun getLocalTime(): String {
        memScoped {
            val time = alloc<time_tVar>()
            time(time.ptr)
            val info = localtime(time.ptr)?.get(0)!!
            val text = allocArray<ByteVar>(20)
            if (info.tm_sec % 2 == 0) sprintf(text, "[%02d:%02d]", info.tm_hour, info.tm_min)
            else sprintf(text, "[%02d %02d]", info.tm_hour, info.tm_min)
            return text.toKString()
        }
    }
}
