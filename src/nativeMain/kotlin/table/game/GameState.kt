package table.game

enum class GameState {
    NotStarted, InProgress, Finished, RestartCooldown
}
