package table.game

import table.SoundPlayer

/**
 * Created by Marcin Wisniowski (Nohus) on 02/03/2020.
 */

class ScoreAnnouncer(
    private val scores: Map<Team, Int>,
    private val soundPlayer: SoundPlayer
) {

    private var announcedScores: List<Int>? = null

    fun announceScores() {
        if (getScoreDigits() != announcedScores) {
            announcedScores = getScoreDigits()
            soundPlayer.play(*getScoresSounds().toTypedArray())
        }
    }

    private fun getScoresSounds(): List<String> {
        return getScoreDigits().mapNotNull { getSound(it) }
    }

    private fun getSound(digit: Int): String? {
        return when (digit) {
            1 -> "digits/one"
            2 -> "digits/two"
            3 -> "digits/three"
            4 -> "digits/four"
            5 -> "digits/five"
            6 -> "digits/six"
            7 -> "digits/seven"
            8 -> "digits/eight"
            9 -> "digits/nine"
            else -> null
        }
    }

    private fun getScoreDigits(): List<Int> {
        val red = scores.getValue(Team.Red)
        val blue = scores.getValue(Team.Blue)
        return listOf(red, blue).sorted()
    }
}
