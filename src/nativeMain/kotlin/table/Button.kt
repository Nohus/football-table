package table

import table.Wire.Mode.*
import table.Wire.PullResistor.*
import table.Wire.State
import table.Wire.State.*
import kotlin.system.getTimeMicros

class Button(
    private val pin: Int,
    private val activationState: State,
    private val pressAction: () -> Unit = {},
    private val longPressAction: () -> Unit = {}
) {

    init {
        Wire.setMode(pin, INPUT)
        if (activationState == HIGH) {
            Wire.setPullResistor(pin, PULL_DOWN)
        } else {
            Wire.setPullResistor(pin, PULL_UP)
        }
    }

    private val debounceTime = 50_000
    private val longPressTime = 1_000_000
    private var isLongPressConsumed = false
    private var currentState = false
    private var lastStateChange = 0L

    fun update() {
        if (isDebouncing()) return

        val state = isPressed()
        if (state != currentState) {
            if (state) isLongPressConsumed = false
            if (!state && !isLongPressConsumed) pressAction()
            lastStateChange = getTimeMicros()
            currentState = state
        }
        if (state && !isLongPressConsumed && isLongPress()) {
            isLongPressConsumed = true
            longPressAction()
        }
    }

    private fun isDebouncing(): Boolean {
        return getTimeMicros() - lastStateChange <= debounceTime
    }

    private fun isLongPress(): Boolean {
        return getTimeMicros() - lastStateChange >= longPressTime
    }

    fun isPressed(): Boolean {
        return Wire.read(pin) == activationState
    }
}
