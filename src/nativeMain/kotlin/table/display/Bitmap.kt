package table.display

import kotlin.math.max

class Bitmap(
    val width: Int,
    val height: Int
) {

    companion object {
        fun from(other: List<List<Int>>): Bitmap {
            val width = other[0].size
            val height = other.size
            return Bitmap(width, height).also {
                for (y in 0 until height) {
                    for (x in 0 until width) {
                        it.bitmap[y][x] = other[y][x] > 0
                    }
                }
            }
        }
    }

    private val bitmap = List(height) {
        MutableList(width) { false }
    }

    operator fun set(x: Int, y: Int, value: Boolean) {
        bitmap[y][x] = value
    }

    operator fun get(x: Int, y: Int): Boolean {
        return bitmap[y][x]
    }

    operator fun set(x: Int, y: Int, other: Bitmap) {
        for (currentX in 0 until other.width) {
            for (currentY in 0 until other.height) {
                set(x + currentX, y + currentY, other[currentX, currentY])
            }
        }
    }

    fun flipped(): Bitmap {
        val new = Bitmap(width, height)
        for (x in 0 until width) {
            for (y in 0 until height) {
                new[x, y] = get(width - 1 - x, height - 1 - y)
            }
        }
        return new
    }

    operator fun plus(other: Bitmap): Bitmap {
        val new = Bitmap(width + other.width, max(height, other.height))
        new[0, 0] = this
        new[width, 0] = other
        return new
    }

    fun forEachLine(block: (Int, List<Boolean>) -> Unit) {
        bitmap.forEachIndexed { index, line -> block(index, line) }
    }

    fun print() {
        for (y in 0 until height) {
            for (x in 0 until width) {
                print(if (this[x, y]) "◼" else "◻")
            }
            println()
        }
        println()
    }

}
