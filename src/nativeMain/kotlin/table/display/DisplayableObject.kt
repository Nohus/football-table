package table.display

import table.display.animations.AnimationFrames
import table.display.animations.Animations
import table.display.bitmaps.Bitmaps
import kotlin.system.getTimeMillis

data class DisplayableObject(
    var bitmap: Bitmap = Bitmaps.EMPTY,
    var x: Int = 0,
    var y: Int = 0,
    var isWrappingAround: Boolean = false,
    val animations: MutableList<AnimationFrames> = mutableListOf()
) {

    val width get() = bitmap.width
    val height get() = bitmap.height
    private var nextAnimationTimestamp = getTimeMillis()
    private var nextAnimationFrame = 0

    fun update() {
        if (animations.size > 0) {
            val frames = animations.first()
            if (getTimeMillis() >= nextAnimationTimestamp) {
                frames.animation(this)
                nextAnimationTimestamp = getTimeMillis() + frames.frameLength
                nextAnimationFrame++
                if (nextAnimationFrame >= frames.frameCount) {
                    nextAnimationFrame = 0
                    animations.removeAt(0)
                }
            }
        }
    }

    fun delayed(millis: Long) = apply {
        animations.add(0, AnimationFrames(Animations.static, 1, millis))
    }

}
