package table.display

import table.display.bitmaps.BitmapDiff

/**
 * Display made out of 4x MAX7219 8x8 LED display drivers
 */
class Display(
    clockPin: Int,
    loadPin: Int,
    dataPin: Int,
    private val isFlipped: Boolean = false,
    private val isDebugPrinting: Boolean = false
) {

    companion object {
        const val WIDTH = 32
        const val HEIGHT = 8
    }

    private val drivers = List(4) {
        Max7219(clockPin, loadPin, dataPin, it, 4)
    }
    val canvas = Canvas(WIDTH, HEIGHT)
    private var currentlyDisplayed = canvas.draw()

    init {
        initializeDisplay()
    }

    fun initializeDisplay() {
        onAllDrivers {
            initializeDriver(this)
            repeat(8) {
                setLine(it, List(8) { false })
            }
        }
        display(currentlyDisplayed)
    }

    private fun initializeDriver(driver: Max7219) {
        driver.apply {
            turnOff()
            setDigitCount(8)
            setDecodeMode(false)
            setDisplayTest(false)
            setIntensity(0)
            turnOn()
        }
    }

    fun update() {
        canvas.update()
        val bitmap = canvas.draw()
            .also { if (isDebugPrinting) it.print() }
            .let { if (isFlipped) it.flipped() else it }
        val diff = BitmapDiff(currentlyDisplayed, bitmap).getDiff()
        if (diff.flatten().any { it }) {
            display(bitmap, diff)
        }
        currentlyDisplayed = bitmap
    }

    private fun display(bitmap: Bitmap, diff: List<List<Boolean>>) {
        displaySegment(bitmap) { lineIndex, driver, segment ->
            val lineNeedsUpdate = diff[lineIndex].subList(driver * 8, (driver + 1) * 8).any { it }
            if (lineNeedsUpdate) drivers[driver].setLine(lineIndex, segment)
        }
    }

    private fun display(bitmap: Bitmap) {
        displaySegment(bitmap) { lineIndex, driver, segment ->
            drivers[driver].setLine(lineIndex, segment)
        }
    }

    private fun displaySegment(bitmap: Bitmap, block: (Int, Int, List<Boolean>) -> Unit) {
        bitmap
            .forEachLine { lineIndex, line ->
                line.chunked(8).forEachIndexed { driver, segment ->
                    block(lineIndex, driver, segment)
                }
            }
    }

    private fun onAllDrivers(block: Max7219.() -> Unit) {
        drivers.forEach(block)
    }

}
