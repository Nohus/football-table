package table.display

/**
 * Drawable collection of displayable objects
 */
class Canvas(
    private val width: Int,
    private val height: Int
) {

    private val objects = mutableListOf<DisplayableObject>()

    fun addObject(added: DisplayableObject) {
        objects += added
    }

    fun clear() {
        objects.clear()
    }

    fun update() {
        objects.apply {
            forEach { it.update() }
        }
    }

    fun draw(): Bitmap {
        val screen = Bitmap(width, height)
        objects.forEach {
            for (y in 0 until it.height) {
                for (x in 0 until it.width) {
                    var currentX = it.x + x
                    var currentY = it.y + y

                    if (it.isWrappingAround) {
                        currentX %= width
                        currentY %= height
                    }

                    if (currentX >= width || currentY >= height
                        || currentX < 0 || currentY < 0) continue

                    if (!screen[currentX, currentY]) {
                        screen[currentX, currentY] = it.bitmap[x, y]
                    }
                }
            }
        }
        return screen
    }

}
