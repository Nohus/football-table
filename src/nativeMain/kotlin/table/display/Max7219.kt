package table.display

import table.Wire
import table.Wire.Mode.OUTPUT
import table.Wire.State
import table.Wire.State.HIGH
import table.Wire.State.LOW

class Max7219(
    private val clockPin: Int,
    private val loadPin: Int,
    private val dataPin: Int,
    private val index: Int,
    private val driverCount: Int
) {

    init {
        Wire.setMode(clockPin, OUTPUT)
        Wire.setMode(loadPin, OUTPUT)
        Wire.setMode(dataPin, OUTPUT)
    }

    companion object {
        // Registers
        const val NO_OP = 0x00
        const val DECODE_MODE = 0x09
        const val INTENSITY = 0x0a
        const val SCAN_LIMIT = 0x0b
        const val SHUTDOWN = 0x0c
        const val DISPLAY_TEST = 0x0f

        fun Int.toBinary(): List<Boolean> {
            val list = Array(8) { false }
            for (i in 7 downTo 0) {
                val mask = 1 shl i
                list[i] = this and mask != 0
            }
            return list.reversed()
        }
    }

    /**
     * 1 digit is equivalent to 8 LED's, 8 digits means 64 LED's
     */
    fun setDigitCount(count: Int) {
        require(count in 1..8) { "Invalid digit count: $count" }
        sendCommand(SCAN_LIMIT, count - 1)
    }

    fun setDecodeMode(enabled: Boolean) {
        sendCommand(DECODE_MODE, if (enabled) 0xFF else 0x00)
    }

    fun setDisplayTest(enabled: Boolean) {
        sendCommand(DISPLAY_TEST, if (enabled) 1 else 0)
    }

    fun setIntensity(intensity: Int) {
        require(intensity in 0..15) { "Invalid intensity value: $intensity" }
        sendCommand(INTENSITY, intensity)
    }

    fun turnOn() = setPower(true)

    fun turnOff() = setPower(false)

    private fun setPower(power: Boolean) {
        sendCommand(SHUTDOWN, if (power) 1 else 0)
    }

    fun setLine(index: Int, line: List<Boolean>) {
        require(index in 0..7) { "Invalid line index: $index" }
        require(line.size == 8) { "Values for 8 LEDs required, got ${line.size}" }

        sendCommand(8 - index, line.reversed())
    }

    private fun sendCommand(register: Int, data: Int) {
        sendCommand {
            sendData((register shl 8) + data)
        }
    }

    private fun sendCommand(register: Int, data: List<Boolean>) {
        sendCommand {
            sendData(register.toBinary().toList() + data)
        }
    }

    private fun sendCommand(commandAction: () -> Unit) {
        Wire.write(loadPin, LOW)
        for (i in (driverCount - 1) downTo 0) {
            if (index == i) commandAction()
            else sendData((NO_OP shl 8) + 0)
        }
        Wire.write(loadPin, HIGH)
    }

    private fun sendData(output: Int) {
        for (i in 15 downTo 0) {
            val mask = 1 shl i
            val digit = if (output and mask != 0) HIGH else LOW
            sendDigit(digit)
        }
    }

    private fun sendData(output: List<Boolean>) {
        for (i in 0..15) {
            val digit = if (output[i]) HIGH else LOW
            sendDigit(digit)
        }
    }

    private fun sendDigit(digit: State) {
        Wire.write(clockPin, LOW)
        Wire.write(dataPin, digit)
        Wire.write(clockPin, HIGH)
    }

}
