package table.display.bitmaps

import table.display.Bitmap

class BitmapDiff(private val a: Bitmap, private val b: Bitmap) {

    init {
        require(a.width == b.width && a.height == b.height) { "Cannot diff Bitmaps of different sizes" }
    }

    fun getDiff(): List<List<Boolean>> {
        val diff = List(a.height) { MutableList(a.width) { false } }
        for (y in 0 until a.height) {
            for (x in 0 until a.width) {
                diff[y][x] = a[x, y] != b[x, y]
            }
        }
        return diff
    }

}
