package table.display.bitmaps

import table.display.Bitmap

object Bitmaps {

    val EMPTY = Bitmap(0, 0)

    fun createBitmap(definition: String): Bitmap {
        return definition.trimIndent().lines()
            .map { line ->
                line.filterNot { it == ' ' }.map { if (it == '◼') 1 else 0 }
            }.let {
                Bitmap.from(it)
            }
    }

    fun getText(text: String): Bitmap {
        return try {
            text.toUpperCase().map { Font[it] }.reduce { acc, bitmap -> acc + Font[' '] + bitmap }
        } catch (e: Exception) {
            return getText("?")
        }
    }
    
}
