package table.display.animations

import table.display.DisplayableObject

typealias Animation = DisplayableObject.() -> Unit
