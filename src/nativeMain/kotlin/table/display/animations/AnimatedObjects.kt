package table.display.animations

import table.display.Canvas
import table.display.DisplayableObject
import table.display.bitmaps.Bitmaps
import table.game.Team

object AnimatedObjects {

    fun displayTextToTeam(canvas: Canvas, text: String, team: Team, delay: Long = 0): Long {
        var time = delay
        text.split(" ").forEachIndexed { index, word ->
                time += 700
                displayWordToTeam(canvas, word, team, delay + 700L * index)
            }
        return time
    }

    private fun displayWordToTeam(canvas: Canvas, word: String, team: Team, delay: Long) {
        canvas.addObject(createScrollingText(word, team == Team.Red).delayed(delay))
    }

    private fun createScrollingText(text: String, isFlipped: Boolean = false): DisplayableObject {
        var bitmap = Bitmaps.getText(text)
        if (isFlipped) bitmap = bitmap.flipped()
        val margin = (32 - bitmap.width) / 2
        val translation = if (isFlipped) Animations.translateUp else Animations.translateDown
        val banner = DisplayableObject(animations = mutableListOf(
            AnimationFrames(translation, bitmap.height, 30),
            AnimationFrames(Animations.static, 1, 250),
            AnimationFrames(translation, 8, 30)
        ))
        banner.x = margin
        banner.y = if (isFlipped) 8 else -bitmap.height
        banner.bitmap = bitmap
        return banner
    }

}
