package table.display.animations

data class AnimationFrames(
    val animation: Animation,
    val frameCount: Int,
    val frameLength: Long
)
