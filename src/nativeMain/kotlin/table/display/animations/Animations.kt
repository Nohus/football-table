package table.display.animations

import table.display.DisplayableObject

object Animations {

    val static = fun DisplayableObject.() {}

    val translateLeft = fun DisplayableObject.() {
        x--
    }

    val translateRight = fun DisplayableObject.() {
        x++
    }

    val translateUp = fun DisplayableObject.() {
        y--
    }

    val translateDown = fun DisplayableObject.() {
        y++
    }

}
