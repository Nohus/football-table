package table

import table.BallDetector.State.*
import table.game.Team

class BallDetector(
    private val sensor: UltrasonicSensor,
    private val name: Team,
    private val triggerMillimeters: Int,
    private val maxTriggerMillimeters: Int,
    private val ballDetectedCallback: () -> Unit
) {

    companion object {
        private const val OBSTRUCTIONS_TO_DETECT = 5
        private const val CLEARS_TO_CLEAR = 50
    }

    enum class State {
        CLEAR, OBSTRUCTING, BALL, CLEARING
    }

    private var state = CLEAR
    private var stateProgress = 0

    fun update() {
        val isObstructed = isObstructed()
        when (state) {
            CLEAR -> {
                if (isObstructed) switchState(OBSTRUCTING)
                else stateProgress++
            }
            OBSTRUCTING -> {
                if (isObstructed) {
                    if (++stateProgress >= OBSTRUCTIONS_TO_DETECT) switchState(BALL)
                } else switchState(CLEAR)
            }
            BALL -> {
                if (isObstructed) stateProgress++
                else switchState(CLEARING)
            }
            CLEARING -> {
                if (isObstructed) switchState(BALL)
                else {
                    if (++stateProgress >= CLEARS_TO_CLEAR) switchState(CLEAR)
                }
            }
        }
    }

    private fun switchState(newState: State) {
        println("[$name] Switched state from $state ($stateProgress) to $newState")
        if (state == CLEARING && newState == CLEAR) println()
        if (state == OBSTRUCTING && newState == BALL) ballDetectedCallback()
        state = newState
        stateProgress = 1
    }

    private fun isObstructed(): Boolean {
        val baseline = sensor.getBaseLine()
        val distance = sensor.getDistanceDebug(10)
        printObservation(distance.first, baseline, distance.second)
        return baseline - distance.first >= triggerMillimeters && distance.first < maxTriggerMillimeters
    }

    private fun printObservation(distance: Long, baseline: Long, measurements: List<Long>) {
        val delta = baseline - distance

        if (distance >= maxTriggerMillimeters) return

        if (delta >= triggerMillimeters) {
            println("[$name] ========== $distance / $baseline ($delta) <-- $measurements")
        } else {
            //println("[$name] |||||||||| $distance / $baseline ($delta) <-- $measurements")
        }
    }

}
