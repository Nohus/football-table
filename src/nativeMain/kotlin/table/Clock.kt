package table

import kotlinx.cinterop.alloc
import kotlinx.cinterop.memScoped
import kotlinx.cinterop.ptr
import platform.posix.gettimeofday
import platform.posix.timeval

class Clock {

    fun getMillis(): ULong {
        memScoped {
            val time = alloc<timeval>()
            gettimeofday(time.ptr, null)
            return 1_000UL * time.tv_sec.toUInt() + (time.tv_usec.toUInt() / 1_000UL)
        }
    }

    fun getMicroseconds(): ULong {
        memScoped {
            val time = alloc<timeval>()
            gettimeofday(time.ptr, null)
            return 1_000_000UL * time.tv_sec.toUInt() + time.tv_usec.toUInt()
        }
    }

}
