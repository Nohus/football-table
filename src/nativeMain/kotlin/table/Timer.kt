package table

class Timer private constructor(private val microseconds: ULong, start: Boolean = true) {

    private val clock = Clock()

    companion object {
        fun ofSeconds(seconds: Int, start: Boolean = true) = Timer(seconds.toULong() * 1_000_000UL, start)
        fun ofMillis(milliseconds: Long, start: Boolean = true) = Timer(milliseconds.toULong() * 1_000UL, start)
        fun ofMicros(microseconds: ULong, start: Boolean = true) = Timer(microseconds, start)
    }

    init {
        if (start) start()
    }

    private var startTime = clock.getMicroseconds()

    fun start() {
        startTime = clock.getMicroseconds()
    }

    fun isRunning() = !isReached()

    fun isReached() = clock.getMicroseconds() - startTime >= microseconds

}
