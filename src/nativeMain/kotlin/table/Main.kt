package table

import table.display.Display
import table.game.Game
import table.game.Team
import table.game.Team.Blue
import table.game.Team.Red
import kotlin.native.concurrent.AtomicInt
import kotlin.native.concurrent.TransferMode
import kotlin.native.concurrent.Worker

// Sensor 1
const val TRIGGER = 14 //8
const val ECHO = 15 //10
// Sensor 2
const val TRIGGER2 = 17 //11
const val ECHO2 = 18 //12
// Display
const val CLOCK = 21 //40
const val LOAD = 20 //38
const val DATA = 16 //36
// Button
const val BUTTON = 12 //32

fun main() {
    println("Starting")

    val soundPlayer = SoundPlayer()
    val display = Display(CLOCK, LOAD, DATA, isFlipped = false, isDebugPrinting = false)
    val game = Game(soundPlayer, display)

    Worker.start().execute(TransferMode.SAFE, { game.pendingBlueGoal }) {
        runBallDetector(TRIGGER, ECHO, Blue, it)
    }
    Worker.start().execute(TransferMode.SAFE, { game.pendingRedGoal }) {
        runBallDetector(TRIGGER2, ECHO2, Red, it)
    }
    while (true) {
        game.update()
        display.update()
    }
}

fun runBallDetector(triggerPin: Int, echoPin: Int, team: Team, goalBuffer: AtomicInt) {
    val sensor = UltrasonicSensor(triggerPin, echoPin)
    val ballDetector = BallDetector(sensor, team, 40, 200) {
        goalBuffer.compareAndSet(0, 1)
    }
    while (true) {
        ballDetector.update()
    }
}
