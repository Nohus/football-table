#!/bin/bash

if [ "$2" == "loop" ]; then
    ./soundloop.sh "$1" &
else
    play $1 > /dev/null 2> /dev/null &
fi

echo "$!"
